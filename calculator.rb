require_relative './exceptions/invalid_input'

class Calculator
  DIGITS_REGEXP = Regexp.new(/(\d+)/)
  NEGATIVE_DIGITS_REGEXP = Regexp.new(/(-\d+)/)

  def add(input_str)
    negative_numbers = input_str.scan(NEGATIVE_DIGITS_REGEXP).flatten

    if !negative_numbers.empty?
      raise InvalidInput.new("Negative numbers not allowed #{negative_numbers.join(',')}")
    end

    input_str.scan(DIGITS_REGEXP).flatten.map(&:to_i).sum
  end
end

